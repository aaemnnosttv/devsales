<?php
/*
Plugin Name: Dev Sales 2.0
Description: Feauturing a rich, beautifully designed interface, DevSales is the only plugin you'll ever need to display your PageLines Store Sales. PageLines Store Authors only.
Version: 1.1
PageLines: true
V3: true
*/

require_once( 'inc/class.devsales-dev-api.php' );

###### INIT #####
if ( is_admin() )
	new DevSales;
#################

class DevSales {

	const version  = '1.1';
	const creds    = 'devsales-creds';
	const settings = 'devsales-settings';

	private $store;
	private $creds;
	private $authenticated;

	function __construct() {

		$this->id = basename( __FILE__, '.php' );

		$this->url = plugins_url( $this->id );
		$this->dir = dirname( __FILE__ );

		$this->hooks = array();

		$this->creds = array(
			'user' => '',
			'key'  => ''
		);

		$this->dev = (defined('DEVSALES_DEV') && DEVSALES_DEV) ? true : false;

		$this->actions();
	} // __construct

	function actions() {
		add_action( 'admin_menu', array(&$this, 'add_menu_pages') );
	}

	function add_menu_pages() {
		$this->hook = add_menu_page( 'Dev Sales', 'Dev Sales', 'delete_users', $this->id, array(&$this, 'build_option_page'), $this->url.'/icon.png' );
		$this->hooks['overview'] = add_submenu_page( $this->id, 'Overview', 'Overview', 'delete_users', $this->id, array(&$this, 'build_option_page') );
		//$this->hooks['single'] = add_submenu_page( $this->id, 'Single Stats', 'Single Stats', 'delete_users', "{$this->id}_single", array(&$this, 'build_option_page') );

		// setup our option page-only actions for all of our pages
		foreach ( $this->hooks as $hook )
			add_action( "load-$hook", 	array(&$this, 'option_page_actions') );
	}


	function option_page_actions() {

		global $hook_suffix;

		// (overview|single)
		$this->page = array_search( $hook_suffix, $this->hooks );

		$this->process_post();
		$this->setup_creds();

		$cache = ( isset( $_GET['refresh'] ) && $_GET['refresh'] ) ? false : true;

		// only load the api on our pages
		$this->api = new DevSalesStoreDevAPI( $this->creds['user'], $this->creds['key'], $cache );

		$this->authenticated = $this->api->is_validated();

		add_action( 'admin_enqueue_scripts',		array(&$this, 'admin_enqueue') );
		add_action( 'admin_print_styles',			array(&$this, 'dynamic_styles') );
		add_filter( 'admin_body_class', 			array(&$this, 'body_classes') );
	}

	function admin_enqueue() {

		$devsales_js = sprintf('devsales%s.js', 	$this->dev ? '' : '.min');

		// styles
		wp_enqueue_style ( 'devsales',				$this->url.'/admin.css', array(), self::version );
		wp_enqueue_style ( 'media' );
		// scripts
		wp_enqueue_script( 'highcharts',			$this->url.'/js/highcharts.js', array(), '3.0.1' );
		wp_enqueue_script( 'highcharts_export',		$this->url.'/js/exporting.js', array(), '3.0.1' );
		wp_enqueue_script( 'bootstrap_tooltips',	$this->url.'/js/jquery.tooltips.js', array('jquery'), '2.3.1' );
		wp_enqueue_script( 'jquery_cookie',			$this->url.'/js/jquery.cookie.js', array('jquery'), '1.3.1' );
		wp_enqueue_script( 'jquery_afterresize', 	$this->url.'/js/jquery.afterresize.min.js', array('jquery'), '1.0' );	
		wp_enqueue_script( 'devsales',				$this->url.'/js/'.$devsales_js, array('jquery','jquery_afterresize','highcharts'), self::version );
		wp_enqueue_script( 'devsales_media_uplder',	$this->url.'/js/devsales.media.uploader.js', array(), self::version);

		$this->setup_graph_data();
		wp_localize_script( 'devsales', 'devsalesGraphData', $this->graph_data );

		// Load Media Scripts
		if ( function_exists('wp_enqueue_media') )
			wp_enqueue_media(); // Load media manager (3.5+)
		// pre 3.5
		else {
			wp_enqueue_script('media-upload');
			wp_enqueue_script('thickbox');
			wp_enqueue_style ('thickbox');
		}
	}

	/**
	 * Set credentials
	 */
	private function setup_creds() {
		$creds = get_option( self::creds );
		$creds = wp_parse_args( $creds, $this->creds );
		$this->creds = $creds;
	}

	private function process_post() {

		if ( empty( $_POST['devsales_nonce'] ) || !wp_verify_nonce( $_POST['devsales_nonce'], self::settings ) )
			return false;

		$p = stripslashes_deep( $_POST );

		$creds = array(
			'user' => isset( $_POST['devsales_username'] )	? $_POST['devsales_username']	: '',
			'key'  => isset( $_POST['devsales_apikey'] )	? $_POST['devsales_apikey']		: ''
		);

		update_option( self::creds, $creds );

		$settings = get_option( self::settings );

		foreach ( $this->get_devsales_option_keys() as $key ) {
			if ( isset( $_POST[ $key ] ) )
				$settings[ $key ] = $p[ $key ];
			else
				$settings[ $key ] = '';
		}

		update_option( self::settings, $settings );
	}

	function get_devsales_option_keys() {
		return array(
			'devsales_bgurl', // more to come probably
		);
	}

	function opt( $key ) {
		$options = get_option( self::settings, array() );
		return isset( $options[ $key ] ) ? $options[ $key ] : false;
	}

	/**
	 * API INTERACTION
	 * #################################################################################################
	 */

	function get_filtered_products_sales_total( $products ) {

		$total = 0;
		if ( !empty( $products ) )
			foreach ( $products as $p )
				$total += $p['sales']['total'];

		return $total;
	}

	function get_filtered_products_sales_count( $products ) {
		$count = 0;
		if ( !empty( $products ) )
			foreach ( $products as $p )
				foreach ( $p['sales'] as $timestamp => $sales )
					$count += count( $sales );

		return $count;
	}

	function range_to_args( $range ) {
		$args = array(
			'sales_start_date' => $range['start'],
			'sales_end_date'   => $range['end'],
		);
		return array_map( array(&$this->api,'get_timestamp'), $args);
	}	

	###########################################################
	#	Month X
	###########################################################
	/**
	 * Returns starting and ending timestamps (GMT) for a given month
	 * Defaults to current month
	 * @param  (int) $relative 		timestamp to use for getting a relative range
	 * @return (array)
	 */
	function get_month_range( $relative = null, $format = 'range' ) {

		$today = $relative ? $relative : current_time('timestamp', true);
		$days = gmdate('t', $today);

		$start = $this->api->get_timestamp( gmdate('M 1 Y', $today) );
		$end   = $start + (DAY_IN_SECONDS * $days) - 1; // -1 ensures end time is last second of previous month

		if ( 'args' == $format )
			$keys = array('sales_start_date','sales_end_date');
		if ( 'range' == $format )
			$keys = array('start','end');

		$values = array( $start, $end );

		return array_combine( $keys, $values );
	}

	function get_month_args( $relative = null ) {
		return $this->get_month_range( $relative, 'args' );
	}

	function get_month_products( $relative = null ) {
		$args = $this->get_month_args( $relative );
		return $this->api->filter_products( $args );
	}

	function get_month_sales( $relative = null ) {
		$args = $this->get_month_args( $relative );
		return $this->api->filter_sales( $args );
	}

	function get_month_sales_count( $relative = null ) {
		$products = $this->get_month_products( $relative );
		return $this->get_filtered_products_sales_count( $products );
	}

	function get_month_sales_total( $relative = null ) {
		$products = $this->get_month_products( $relative );
		return $this->get_filtered_products_sales_total( $products );
	}

	function get_month_sales_data( $stamp = null ) {
		return array(
			'total' => $this->get_month_sales_total( $stamp ),
			'count' => $this->get_month_sales_count( $stamp ),
			'month' => $this->get_month_name_from_timestamp( $stamp )
		);
	}		

	###########################################################
	#	Current month
	###########################################################

	function get_current_month_range() {
		return $this->get_month_range();
	}

	function get_current_month_args() {
		return $this->get_month_args();
	}

	function get_current_month_products() {
		return $this->get_month_products();
	}

	function get_current_month_sales() {
		return $this->get_month_sales();
	}

	###########################################################
	#	Previous month
	###########################################################
	
	/**
	 * Returns args array with timestamps for the beginning and end of the previous month
	 * Whole month
	 * @return [type] [description]
	 */
	function get_prev_month_range( $relative = null ) {
		$month = $this->get_month_range( $relative );
		$lm_stamp = $month['start'] - DAY_IN_SECONDS; // current month - 1 day

		return $this->get_month_range( $lm_stamp ); // prev month range
	}

	function get_prev_month_args( $relative = null ) {
		return $this->get_prev_month_range( $relative, 'args' );
	}

	/**
	 * Returns total sales in $ for previous month
	 * @return [type] [description]
	 */
	function get_prev_month_total( $relative = null ) {
		$last_month_products = $this->get_prev_month_products( $relative );
		return $this->get_filtered_products_sales_total( $last_month_products );
	}

	function get_prev_month_sales_count( $relative = null ) {
		$last_month_products = $this->get_prev_month_products( $relative );
		return $this->get_filtered_products_sales_count( $last_month_products );
	}

	function get_prev_month_products( $relative = null ) {
		$last_month_args = $this->get_prev_month_args( $relative );
		return $this->api->filter_products( $last_month_args );
	}

	###########################################################
	#	Previous month-to-date
	###########################################################

	/**
	 * Returns total sales in $ for previous month-to-date
	 * @return [type] [description]
	 */
	function get_prev_mtd_total() {
		$last_mtd_products = $this->get_prev_mtd_products();
		return $this->get_filtered_products_sales_total( $last_mtd_products );
	}

	function get_prev_mtd_products() {
		$last_mtd_args = $this->get_prev_mtd_args();
		return $this->api->filter_products( $last_mtd_args );
	}

	/**
	 * Get *previous* month-to-date args for filtering products
	 * ie: today = 5/14  would return ~ 4/1 - 4/14 
	 * @return array 	start/end timestamps GMT
	 */
	function get_prev_mtd_args() {

		$today = current_time('timestamp', true);
		$month = $this->get_current_month_range();
		$prevm = $this->get_prev_month_range();

		$cm_stamp = $month['start'];
		$lm_stamp = $prevm['start'];

		$cur_m    =       date('M', $cm_stamp); // current month name
		$cur_day  = (int) date('j', $today   ); // current day num
		$cur_y    = (int) date('Y', $cm_stamp); // current year

		$lm_m     =       date('M', $lm_stamp); // last month name
		$lm_y     = (int) date('Y', $lm_stamp); // last month's year
		$lm_days  = (int) date('t', $lm_stamp); // last month total days

		$start_date = $lm_stamp;

		// possible end dates
		$prop_end_date = $lm_stamp + ( $cur_day * DAY_IN_SECONDS ); // "current" day of previous month
		$safe_end_date = $cm_stamp; // first day of current month

		// use the 1st of the current month as the end date if the previous month has fewer days ex: feb: 28 / mar: 31
		// otherwise, use the proportionate end date to get accurate sales from the same month segment
		$end_date = ( $lm_days < $cur_day ) ? $safe_end_date : $prop_end_date;

		return array(
			'sales_start_date' => $start_date,
			'sales_end_date'   => $end_date,
		);
	}

	/**
	 * Return formatted take-home month-to-date sales total $$
	 * @return [type] [description]
	 */
	function get_mtd_sales() {
		$total = $this->get_mtd_total();
		return $this->format_total( $total );
	}

	/**
	 * Return gross month-to-date sales total $$
	 * @return [type] [description]
	 */
	function get_mtd_total() {
		$current_month_args = $this->get_current_month_args();
		$products = $this->api->filter_products( $current_month_args );
		$total = $this->get_filtered_products_sales_total( $products );

		return $total;
	}

	/**
	 * Calculates the difference between the current month's sales performance with the previous month's
	 * Based on sales total $, not number of transactions
	 *
	 * @todo  allow relative
	 *
	 * @return (mixed)		difference as a decimal percentage if there are previous month sales to compare with
	 *                      otherwise FALSE
	 */
	function get_month_sales_diff() {

		$products = $this->api->get_products();
		if ( empty( $products ) )
			return 0;

		$last_month = $this->get_prev_mtd_total();
		$this_month = $this->get_mtd_total();

		if ( $this_month && $last_month ) {
			$diff = $this_month / $last_month;
			$change = $diff - 1; // 0 = same as last month
		}
		else
			$change = false;


		return $change;
	}

	###########################################################
	#	Multi-month
	###########################################################

	/**
	 * Return formatted total sales (in dollars)
	 * @return string HTML
	 */
	function get_all_time_sales_total() {
		$r = $this->api->request('developer-total');
		$total = isset( $r['total'] ) ? $r['total'] : 0;
		return $this->format_total( $total );
	}

	/**
	 * Return formatted total year-to-date sales (in dollars)
	 * @return string HTML
	 */
	function get_ytd_sales() {

		$total = 0;

		if ( $this->authenticated ) {
			$r = $this->api->request( 'developer-total', array('days' => 'ytd') );
			$total = isset( $r['total'] ) ? $r['total'] : 0;
		}

		return $this->format_total( $total );
	}



	/**
	 * DISPLAY
	 * #################################################################################################
	 */

	/**
	 * Master callback for rendering option page markup
	 */
	function build_option_page() {
		$this->print_markup('common');		
	}

	/**
	 * Output markup from another file
	 * @param  (string) 	$file 	ie: markup.{$file}.php
	 */
	function print_markup( $file ) {
		echo $this->get_markup( $file );
	}

	/**
	 * Return markup from another file
	 * @param  (string) 	$file 	ie: markup.{$file}.php
	 * @return mixed       	string - html if file found
	 *                      null if file not found & debug message output
	 */
	function get_markup( $file ) {
		$filename = "{$this->dir}/inc/markup.$file.php";
		if ( file_exists( $filename ) ) {
			ob_start();
			require_once $filename;
			return ob_get_clean();
		}
		else {
			devsales_debug( $filename, 'File not found');
			return null;
		}
	}

	function build_greeting() {
		$creds = array_filter( $this->creds );
		$greeting_text = $this->get_greeting_text();

		if ( $this->authenticated )
			$msg = "<span class='big-greeting'>$greeting_text, {$this->creds['user']}.</span>";
		elseif ( empty( $creds ) )
			$msg = "$greeting_text! <span class='smaller'>To begin, enter your credentials.</span>";
		else
			$msg = 'Uh oh. Something went wrong.';

		return $msg;
	}

	static function format_total( $gross ) {
		return sprintf('<span rel="tooltip" data-placement="right" title="%s Gross">%s</span>', round( $gross ), round( self::get_takehome( $gross ) ) );
	}

	static function get_takehome( $gross ) {
		return $gross * 0.7;
	}

	/**
	 * Calculates & generates markup for displaying sales performance (in %) comparison to the same time last month
	 * @return string HTML
	 */
	function get_month_sales_comp() {

		$diff = $this->get_month_sales_diff();

		if ( false === $diff )
			return '<span class="mtd-comp no-data"><span rel="tooltip" data-placement="right" title="Not enough data to compare.">N/A</span></span>';

		$percentage = abs( round( $diff * 100, 2 ) );

		$class = '';
		$symbol = '';

		if ( 0 !== $diff ) {
			$symbol = ( $diff > 0 ) ? '<i class="devsalesicon-up-micro"></i>' : '<i class="devsalesicon-down-micro"></i>';
			$class = ( $diff > 0 )  ? 'positive' : 'negative';
		}

		return sprintf('<span class="mtd-comp %s-comp"><span class="symbol">%s</span> <span class="value">%s <span class="per">&#37;</span></span></span>',
			$class,
			$symbol,
			$percentage
		);
	}

	/**
	 * Dynamically add admin body classes for our option pages
	 * @param  [type] $c [description]
	 * @return [type]    [description]
	 */
	function body_classes( $c ) {

		$add = array(
			$this->get_time_of_day(),
			sprintf(' devsales-%s', $this->page )
		);

		$c = trim( $c ) . ' ' . implode(' ', $add);

		return $c;
	}

	function dynamic_styles() {

		$bg_opt = $this->opt('devsales_bgurl');
		$bg = $bg_opt ? $bg_opt : $this->get_time_of_day_bg_url();
		?>
		<style type="text/css">
			#wpwrap {
				background-image: <?php printf('url( %s );', $bg ); ?>
				background-size: cover;
				background-position: 50% 50%;
			}
		</style>
		<?php
	}

	function get_time_of_day_bg_url() {
		return sprintf('%s/img/%s.jpg', $this->url, $this->get_time_of_day() );
	}
	###########################################################
	#	Ticker
	###########################################################

	/**
	 * Output ticker list items
	 */
	function sb_ticker() {

		$sales = $this->api->get_sales();

		if ( empty( $sales ) )
			return;

		$sales = $this->add_ticker_events( $sales );

		krsort( $sales ); // sort high to low

		$recent = array_slice( $sales, 0, 20, true ); // preserve keys

		foreach ( $recent as $timestamp => $set )
			foreach ( $set as $data )
				$this->ticker_item_switch( $data, $timestamp );
	}

	/**
	 * Adds payout range boundaries and payout events for the last 3 months
	 * @param [type] $sales [description]
	 */
	function add_ticker_events( $sales ) {

		$today = current_time('timestamp', true);
		if ( 15 <= (int) date('j', $today) ) {
			$payout = $this->api->get_timestamp( date('M 15, Y', $today) );
			$sales[ $payout ][] = array('datatype' => 'payout');
		}

		$stamp = '';
		for ( $i = 1; $i <= 3; $i++ ) {
			$range = $this->get_prev_month_range( $stamp );
			$stamp = $range['end'];
			$sales[ $stamp ][] = array('datatype' => 'boundary');
			$payout = $this->api->get_timestamp( date('M 15, Y', $stamp) );
			$sales[ $payout ][] = array('datatype' => 'payout');
		}

		return $sales;
	}

	/**
	 * Handles ticker item display based on the item's datatype
	 * @param  [type] $data      [description]
	 * @param  [type] $timestamp [description]
	 * @return [type]            [description]
	 */
	function ticker_item_switch( $data, $timestamp ) {
		switch ( $data['datatype'] ) {
			case 'payout' :
				$this->do_payday_ticker_item( $timestamp );
				break;

			case 'boundary' :
				$this->do_boundary_ticker_item( $timestamp );
				break;

			case 'sale' :
				$this->do_product_sale_ticker_item( $data, $timestamp );
				break;
		}
	}

	/**
	 * Master ticker item printer thing
	 * @param  [type] $stamp        [description]
	 * @param  string $data         [description]
	 * @param  string $img_src      [description]
	 * @param  string $item_classes [description]
	 * @return [type]               [description]
	 */
	function do_ticker_item( $stamp, $data = '', $img_src = '', $item_classes = 'product-sale' ) {
		$img_src = $img_src ? $img_src : 'http://placehold.it/40x31';
		printf('<li class="%s">
					<img class="sb-ticker-thumb" src="%s" />
					<span class="sb-ticker-time">%s ago</span>
					<span class="sb-ticker-product">%s</span>
				</li>',
			esc_attr( $item_classes ),
			esc_attr( $img_src ),
			human_time_diff( $stamp, current_time( 'timestamp', true ) ),
			$data // the bottom "main" line
		);
	}

	/**
	 * Outputs a product sale ticker item
	 * @param  [type] $data      [description]
	 * @param  [type] $timestamp [description]
	 * @return [type]            [description]
	 */
	function do_product_sale_ticker_item( $data, $timestamp ) {
		$name = $this->get_pinfo( $data['product'], 'name' );
		$img = $this->get_pinfo( $data['product'], 'image' );
		// do the item!
		$this->do_ticker_item( $timestamp, $name, $img );
	}

	/**
	 * Outputs payday data as a single ticker list item
	 * @param  [type] $stamp [description]
	 * @return [type]        [description]
	 */
	function do_payday_ticker_item( $stamp ) {

		$prev = $this->get_prev_month_range( $stamp );
		$data = $this->get_month_sales_data( $prev['start'] );

		$output = sprintf('<span class="payout-title">%s Payout</span> &mdash; <span class="payout-meta"><span class="currency">$%s</span> | <span class="count">%s sales</span></span>',
			$data['month'],
			$this->format_total( $data['total'] ),
			$data['count']
		);

		$this->do_ticker_item( $stamp, $output, $this->url.'/img/payout-thumb.png', 'payout' );
	}

	/**
	 * Outputs a ticker item for the mark of a new pay period
	 * @param  [type] $stamp [description]
	 * @return [type]        [description]
	 */
	function do_boundary_ticker_item( $stamp ) {
		$data = $this->get_month_sales_data( $stamp );
		$name = $this->get_month_name_from_timestamp( $stamp );
		$output = sprintf('%s &mdash; end pay period | <span class="payout-meta"><span class="currency">$%s</span> | <span class="count">%s sales</span></span>',
			$name,
			$this->format_total( $data['total'] ),
			$data['count']
		);

		$this->do_ticker_item( $stamp, $output, $this->url.'/img/new-pay-period.png', 'boundary');
	}

	/**
	 * Output sorted individual product sales count totals for month-to-date
	 * (Month in view)
	 * @return (float)	$total 	total mtd sales count
	 */
	function the_sb_list() {

		$products = $this->get_current_month_products();
		if ( empty( $products ) )
			return;

		$list = array();
		$total = 0;
		foreach ( $products as $p ) {
			$count = count( $p['sales']['items'] );
			$total += $count;
			if ( $count ) // don't show products w/ 0 sales
				$list[ $count ][] = $p;
		}

		unset( $p );

		// sort by number of sales descending
		krsort( $list );

		// prods = array of products with same sale count
		foreach ( $list as &$prods ) {

			if ( count( $prods ) > 1 ) {

				$sort = array();
				foreach ( $prods as $p )
					$sort[ $p['slug'] ] = $p['name'];

				unset( $p );

				asort( $sort ); // sort by name - ascending

				$new = array();
				foreach ( $sort as $slug => $pname )
					$new[] = $products[ $slug ];

				unset( $sort );

				$prods = $new;
				unset( $new );
			}

			foreach ( $prods as $p )
				$this->print_side_li( $p );
		}

		return $total;
	}

	/**
	 * Output month in view list item
	 * @param  (array) 	$p 	api product data array
	 */
	function print_side_li( $p ) {
		printf( '<li>%s <span class="devsales-sb-unit-integer">%s</span></li>',
			$p['name'],
			count( $p['sales']['items'] )
		);
	}

	function graph_unit() {

		if ( ! $this->authenticated )
			return '';

		ob_start();
		?>
		<div class="devsales-graph-unit">
			<div class="devsales-graph-unit-pad">
				<div id="the_graph"></div>
			</div>
		</div>
		<?php

		return ob_get_clean();
	}

	function get_greeting_text() {

		$time_of_day = $this->get_time_of_day();
		$text = sprintf('Good %s', $time_of_day );
		$message = apply_filters( 'devsales_greeting_text', $text, $time_of_day );

		return $message;
	}

	/**
	 * GRAPH INTERACTION
	 * #################################################################################################
	 */	

	function setup_graph_data() {

		$data = array();
		$method = "get_{$this->page}_graph_data";

		if ( method_exists( $this, $method ) )
			$data = $this->$method();

		$this->graph_data = $data;
	}

	/**
	 * Returns Highcharts configuration for Overview page graph
	 * Displays daily sales activity for the current month-to-date 
	 * @return array
	 */
	function get_overview_graph_data() {

		$config = array();
		$config['chart']['type'] = 'line';
		$config['title']['text'] = 'Sales Activity';
		$config['xAxis']['categories'] = $this->get_mtd_days();
		$config['yAxis']['title'] = array( 'text' => 'Daily Count' );
		$config['series'] = $this->get_overview_series();
		$config['credits']['enabled'] = false;
		// $config['tooltip'] = array();
		// $config['___'] = array();

		return $config;
	}

	/**
	 * Returns Highcharts configuration for the Single product page graph
	 * 
	 * @todo  this
	 * 
	 * @return array
	 */
	function get_single_graph_data() {
		$config = array();
		/*$config['chart']['type'] = 'line';
		$config['title']['text'] = 'Product Sales';
		$config['xAxis']['categories'] = $this->get_mtd_days();
		$config['yAxis']['title'] = array( 'text' => 'Sales' );
		$config['series'] = $this->get_overview_series();
		$config['credits']['enabled'] = false;
		$config['tooltip'] = array();
		$config['legend'] = array(
			'x'             => -10,
			'y'             => 100,
			'layout'        => "vertical",
			'align'         => "right",
			'verticalAlign' => "top",
			'borderWidth'   => 0
		);*/
		// $config['___'] = array();

		return $config;
	}


	/**
	 * Get Highcharts series data for overview graph
	 * @return (array)
	 */
	function get_overview_series() {

		$sales = $this->get_current_month_sales();
		$series = array();

		if ( empty( $sales ) )
			return $series;

		$days   = (int) date('j', current_time('timestamp', true) );
		$month  = $this->get_current_month_range();
		$mstart = $month['start'];

		$data = array();
		// loop through each day
		for ( $i = 0; $i < $days; $i++ ) {
			// reset
			$day_count = 0;
			// increment timestamps
			$start = $mstart + ( $i * DAY_IN_SECONDS );
			$end   = $start + DAY_IN_SECONDS;

			// check against each sale time
			foreach ( $sales as $saletime => $saledata ) {
				if ( $start <= $saletime && $saletime < $end ) {
					// loop through and count sales
					$day_count += count( $saledata );
					unset( $sales[ $saletime ] ); // make things quicker
				}
			}

			$data[ $i ] = $day_count; 
		}
		
		$series[] = array(
			'name'    => 'Month to date',
			'data'    => $data,
		);

		return $series;
	}

	/**
	 * Old method for overview series
	 * to be used on single page
	 * @return (array)
	 */
	function _get_overview_series() {

		$products = $this->get_current_month_products();
		$series = array();

		if ( !empty( $products ) ) {
			$products = array_reverse( $products ); // sort newest to oldest
			$slugs = array_keys( $products );

			foreach ( $products as $slug => $p ) {
				$series[] = array(
					'name'    => $p['name'],
					'data'    => $this->get_product_mtd_series_data( $p ),
					'visible' => ( $slug == $slugs[0] ) ? true : false
				);
			}
		}

		return $series;
	}

	/**
	 * Get product sales counts for the current month to date (each day)
	 *
	 * @param  [type] $p  Product array - prefiltered to only contain MTD sales
	 * @return (array) 		indexed array of per-day sales count
	 */
	function get_product_mtd_series_data( $p ) {
		$days = (int) date('j', current_time('timestamp', true) );
		$month = $this->get_current_month_range();
		$mstart = $month['start'];

		$sales = $this->api->get_sales_from_product( $p );

		$data = array();
		for ( $i = 0; $i < $days; $i++ ) {
			// reset
			$day_count = 0;
			// increment timestamps
			$start = $mstart + ( $i * DAY_IN_SECONDS );
			$end = $start + DAY_IN_SECONDS;
			// check against each sale
			foreach ( $sales as $saletime => $saledata ) {
				if ( $start <= $saletime && $saletime < $end ) {
					$day_count += count( $saledata );
					unset( $sales[ $saletime ] ); // make things quicker
				}
			}

			$data[] = $day_count;
		}

		return $data;
	}

	/**
	 * Helper
	 * #################################################################################################
	 */	

	function get_time_of_day() {

		// for testing
		if ( $this->dev && isset( $_GET['tod'] ) )
			return $_GET['tod'];

		$time = current_time('timestamp');
		$today = date( 'M j, Y', $time );

		$morning   = $this->api->get_timestamp( $today );
		$afternoon = $this->api->get_timestamp("$today 12:00pm");
		$evening   = $this->api->get_timestamp("$today 5:00pm");

		if ( $morning <= $time && $time < $afternoon )
			return 'morning';
		elseif ( $afternoon <= $time && $time < $evening )
			return 'afternoon';
		else
			return 'evening';
	}

	/**
	 * Return month-to-date days
	 * @return (array) 	indexed array of mtd days as timestamps
	 */
	function get_mtd_days() {

		$today = current_time('timestamp', true);
		$first = strtotime( date('M 1, Y', $today) );

		// j	Day of the month without leading zeros
		$end = (int) date('j', $today);

		// build array of days as timestamps
		$days = array();
		for ( $i = 1; $i <= $end; $i++ )
			$days[] = $i;
			//$days[] = $first + ( DAY_IN_SECONDS * $i );

		return $days;

		// convert timestamps to readable
		// too much text
		/*foreach ( $days as &$day )
			$day = date( get_option('date_format'), $day ); // format using defined date format

		return $days;*/
	}

	/**
	 * Helper wrapper function for the API get_product_info()
	 * @param  [type] $slug [description]
	 * @param  [type] $key  [description]
	 * @return [type]       [description]
	 */
	function get_pinfo( $slug, $key ) {
		return $this->api->get_product_info( $slug, $key );
	}

	function get_month_name( $m ) {
		$mos = array(
			'', // 0
			'Jan',
			'Feb',
			'Mar',
			'Apr',
			'May',
			'Jun',
			'Jul',
			'Aug',
			'Sep',
			'Nov',
			'Dec'
		);
		return $mos[ $m ];
	}

	/**
	 * Returns full Month name from timestamp
	 * 
	 * @param  (int) 	$stamp  	timestamp
	 * @param  (string) $format 	date format string - defaults to full month name
	 * @return (string)				formatted date
	 */
	function get_month_name_from_timestamp( $stamp, $format = 'F' ) {
		return gmdate( $format, $stamp );
	}	

	// Get the current WP version. used in admin_enqueue
	function get_wp_version() {
	   return $GLOBALS['wp_version'];
	}
} // DevSales

// Debug helpers
function devsales_debug( $data, $title = false, $echo = false ) {
	if ( function_exists('ddprint') )
		ddprint( $data, $title, $echo );
	elseif ( function_exists('plprint') )
		plprint( $data, $title, $echo );
}

function ds_debug_date_array( $a, $label = '' ) {
	$data = array_map('ds_debug_date_f', $a);
	devsales_debug( $data, $label );	
}

function ds_debug_date_f( $stamp ) {
	return date('M j', $stamp);
}