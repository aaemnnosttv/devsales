<?php
/**
 * Overview Page Markup
 * 
 */
?>

<!-- greeting -->
<div class="greeting"><?php echo $this->build_greeting(); ?></div>
<!-- center unit -->
<div class="devsales-center-unit">
	<div class="devsales-center-unit-pad fix">
		<div class="devsales-center-left">
			<span class="devsales-center-label">Total sales</span>
			<span class="devsales-center-integer">
				<span class="ds-dollar-sign">$</span><?php echo $this->get_all_time_sales_total(); ?>
			</span>
			<span class="devsales-center-label set-two">ytd</span>
			<span class="devsales-center-integer set-two">
				<span class="ds-dollar-sign">$</span><?php echo $this->get_ytd_sales(); ?>
			</span>
		</div>
		<div class="devsales-center-center">
			<div class="devsales-center-circle">
				<span class="devsales-center-total"><?php echo $this->get_mtd_sales(); ?></span>
				<span class="devsales-center-total-text">$ this month</span>
			</div>
		</div>
		<div class="devsales-center-right">
			<span class="devsales-center-label">this month</span>
			<span class="devsales-center-integer"><?php echo $this->get_month_sales_comp(); ?></span>
			<span class="devsales-center-label set-two">products</span>
			<span class="devsales-center-integer set-two"><?php echo count( $this->api->get_products() ); ?></span>
		</div>
	</div>
</div>
<!-- graph unit -->
<?php echo $this->graph_unit(); // only outputs if authenticated ?>