<?php
/**
 * Settings & Modals
 */
?>

<!-- buttons -->
<div class="devsales-settings">
	<a class="devsales-open-settings"	href="#devsales-settings-modal" rel="tooltip" data-placement="right" title="Settings"></a>
	<a class="devsales-refresh-api"		href="<?php echo add_query_arg(array('refresh' => 1)); ?>" data-placement="left" rel="tooltip" title="Refresh API"></a>
	<a class="devsales-open-launchpad" 	href="#devsales-launchpad-modal" rel="tooltip" data-placement="right" title="Launchpad"></a>
	<a class="devsales-open-forum" 		href="#devsales-forum-modal" rel="tooltip" data-placement="right" title="Community Forum"></a>
	<?php //<a class="devsales-single-stats" href="#devsales-single-stats" rel="tooltip" data-placement="right" title="Single Product Status"></a> ?>
</div>
<!-- forum -->
<div id="devsales-forum-modal" class="forum-modal">
	<div class="devsales-settings-modal-content-wrap">
		<section class="devsales-settings-content-inner">
			<iframe id="forumframe" src="" width="100%" height="500"></iframe>
		</section>
	</div>
</div>
<!-- launchpad -->
<div id="devsales-launchpad-modal" class="launchpad-modal">
	<div class="devsales-settings-modal-content-wrap">
		<section class="devsales-settings-content-inner">
			<iframe id="frame" src="" width="100%" height="500"></iframe>
		</section>
	</div>
</div>
<!-- settings -->
<div id="devsales-settings-modal">
	<div class="devsales-settings-modal-content-wrap">

		<nav class="devsales-settings-content-nav">
			<a class="devsales-nav-tab-creds active" href="#devsales-cred-tab-content">Login</a>
			<a class="devsales-nav-tab-settings" href="#devsales-settings-tab-content">Settings</a>
			<a class="devsales-nav-tab-info" href="#devsales-info-tab-content">Info</a>
		</nav>

		<section class="devsales-settings-content-inner">
			<form method="post" class="form-horizontal">
				<!-- Settings -->
				<div id="devsales-settings-tab-content">
					<?php wp_nonce_field( self::settings, 'devsales_nonce' ); ?>
					<div class="control-group">
						<label class="control-label" for="input_bgurl">Background Image</label>
						<div class="controls">
							<?php printf('<input type="text" id="input_bgurl" class="input-xlarge" name="%s" placeholder="" value="%s">',
								'devsales_bgurl',
								$this->opt( 'devsales_bgurl' )
							); ?>
							<a href="javascript:;" class="devsales-bg-option">Open Media Uploader</a>
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							<button type="submit" class="devsales-button">Save</button>
						</div>
					</div>
				</div>
				<!-- Login -->
				<div id="devsales-creds-tab-content">
					<div class="control-group">
						<label class="control-label" for="inputEmail">Username</label>
						<div class="controls">
							<input class="input-xlarge" type="text" id="inputUsername" name="devsales_username" placeholder="Username" value="<?php echo $this->creds['user']; ?>"></div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputAPIKey">API Key</label>
						<div class="controls">
							<input class="input-xlarge" type="password" id="inputAPIKey" name="devsales_apikey" placeholder="API Key" value="<?php echo $this->creds['key']; ?>"></div>
					</div>
					<div class="control-group">
						<div class="controls">
							<button type="submit" class="devsales-button">Save</button>
						</div>
					</div>
				</div>
				<!-- Info -->
				<div id="devsales-info-tab-content" class="devsales-info-tab-inner">
					<p class="devsales-tab-bold fivemb">Welcome to DevSales!</p>
					<p class="zmt">To operate this amazing piece of equipment, you'll first need an API key along with your Launchpad Username. Click out of here, and on that rocket icon on the left, login to Launchpad, then find the API key tab. There you'll see your API key. Keep this a secret! Unless you want the whole world knowing your sales info.</p>
					<p class="devsales-tab-bold fivemb">Overview</p>
					<p class="zmt">We really wanted to make the dashboard a personalized experience. The background, and greeting, will change with morning, afternoon, and evening. These changes occur at noon, and 5 pm. DevSales uses the time you have set in the Wordpress install that this plugin is installed in. Make sure you have the correct time set under Settings-->General in Wordpress admin.</p>
					<p>You can optionally override this, and set your own custom background image using the 'Settings' tab above.</p>
					<p class="devsales-tab-bold fivemb">Center Unit</p>
						<ul class="devsales-tab-list">
							<li>Total Sales: what you've made since the beginning of time</li>
							<li>YTD: what you've made since the start of the current year</li>
							<li>This Month: what you've made this month so far</li>
							<li>Change: percentage change from the prior months total</li>
							<li>Products: Shows a count of your active store products</li>
						</ul>
					<p class="devsales-tab-bold fivemb">Graph</p>
					<p class="zmt">The graph shows all your products. By default, all are invisible, except for the first one. Each products graph can be toggle into view by clicking on the product. You can optionally save an image of your graph by using the export feature in the upper right hand corner of the graph.</p>
					<p class="devsales-tab-bold fivemb">Sidebar</p>
					<p class="zmt">The sidebar shows 2 things; a sales ticker, and a "month-in-view" snapshot. The ticker shows the last sale. Note that the products are timestamped with GMT so they are off by a few hours. The 'Month in View' gives a list of your products sold this month, sorted by the most sold. It will not show products with 0 sales, and will also not show inactive products.</p>
					<p class="devsales-tab-bold fivemb">Donate</p>
					<p class="zmt">Like what we've done? Why not donate a few bucks towards beer?</p>
					<p>Paypal email evanmattson@gmail.com</p>
					<p>Note: Our info is only as accurate as the API. If the info is wrong, it's the API. All we're doing is displaying the data retrieved from the PageLines Store API. This epic piece of awesome-sauce is brought to you by <a href="http://twitter.com/nphaskins" target="_blank">@nphaskins</a> and <a href="http://twitter.com/aaemnnosttv" target="_blank">@aaemnnosttv</a>.<br /> Log bugs/requests <a href="https://bitbucket.org/beardedavenger/devsales/issues?status=new&status=open" target="_blank">here</a>.</p>
				</div>
			</form>
		</section>
	</div>
</div>
<div id="devsales-settings-modal-backdrop"></div>